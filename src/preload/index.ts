/** 
 * Copyright 2019 Karl F. Meinkopf
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { ipcRenderer } from 'electron';

const selectPrevButton = () =>
	document.querySelector('.player-controls__btn_prev');
const selectPlayButton = () =>
	document.querySelector('.player-controls__btn_play');
const selectNextButton = () =>
	document.querySelector('.player-controls__btn_next');

const clickEvent = () => new MouseEvent('click');

window.onload = () => {
	const playButton = selectPlayButton();
	const prevButton = selectPrevButton();
	const nextButton = selectNextButton();

	ipcRenderer
		.on(
			'play-pause',
			() => playButton && playButton.dispatchEvent(clickEvent())
		)
		.on(
			'rewind',
			() => prevButton && prevButton.dispatchEvent(clickEvent())
		)
		.on(
			'forward',
			() => nextButton && nextButton.dispatchEvent(clickEvent())
		);
};

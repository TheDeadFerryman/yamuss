/**
 * Copyright 2019 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Menu, BrowserWindow, app } from 'electron';
import strings from '../locales';

const onQuit = (window: BrowserWindow) => () => {
	window.destroy();
	app.quit();
};

const contextMenu = (window: BrowserWindow) =>
	Menu.buildFromTemplate([
		{ label: strings.tray.menu.quit, click: onQuit(window) }
	]);

export default contextMenu;

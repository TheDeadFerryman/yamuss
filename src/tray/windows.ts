/**
 * Copyright 2019 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BrowserWindow, Tray, Menu, app } from 'electron';
import { getIconPath } from '../icon';
import strings from '../locales';
import contextMenu from './contextMenu';

const popupSize = {
	width: 320,
	height: 120
};

const createTray = (window: BrowserWindow) => {
	const tray = new Tray(getIconPath());
	let popup: BrowserWindow | null = null;

	tray.setToolTip(strings.tray.tooltip);
	tray.setContextMenu(contextMenu(window));

	tray.on('double-click', () => {
		window.show();
		tray.destroy();
	});

	tray.on('click', (_, __, { x, y }) => {
		const { width, height } = popupSize;

		if (!popup || popup.isDestroyed) popup = createPopupWindow();

		popup.setPosition(x - width, y - height);
		popup.show();
	});

	return tray;
};

const createPopupWindow = () => {
	const { width, height } = popupSize;

	const window = new BrowserWindow({
		width: width,
		height: height,
		show: false,
		frame: false,
		alwaysOnTop: true,
		type: 'toolbar'
	});

	window.loadFile('layouts/popup.html');

	window.on('blur', () => window.hide());

	return window;
};

export default createTray;

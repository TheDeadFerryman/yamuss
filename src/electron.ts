/** 
 * Copyright 2019 Karl F. Meinkopf
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { app, BrowserWindow } from 'electron';
import strings from './locales/ru-RU';
import createTray from './tray';
import { join } from 'path';
import registerKeybindings from './keybindings';

const createWindow = () => {
	const preload = join(__dirname, 'preload/index.js');

	const window = new BrowserWindow({
		webPreferences: { preload },
		width: 1600,
		height: 900
	});

	window.loadURL(strings.url);

	window.on('close', function(event) {
		event.preventDefault();
		window.hide();
		createTray(window);
	});

	registerKeybindings(window);

	return window;
};

app.on('ready', createWindow);

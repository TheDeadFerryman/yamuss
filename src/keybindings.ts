/**
 * Copyright 2019 Karl F. Meinkopf
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { globalShortcut, BrowserWindow, dialog } from 'electron';

const KEYMAP = {
	MediaPlayPause: 'play-pause',
	MediaPreviousTrack: 'rewind',
	MediaNextTrack: 'forward'
};

const registerKeybindings = (window: BrowserWindow) => {
	globalShortcut.register('CommandOrControl+Alt+Shift+R', () =>
		window.show()
	);

	Object.entries(KEYMAP).forEach(([key, event]) =>
		globalShortcut.register(key, () => window.webContents.send(event))
	);

	[...Object.keys(KEYMAP), 'CommandOrControl+Alt+Shift+R'].forEach(
		key =>
			!globalShortcut.isRegistered(key) &&
			dialog.showMessageBoxSync(window, {
				type: 'warning',
				buttons: ['OK', 'Cancel'],
				title: 'Failed to bind keyboard shortcut!',
				message: `Failed to bind action for '${key}' keyboard shortcut. You can ignore this message and use application without this bind.`,
				cancelId: 1
			})
	);
};

export default registerKeybindings;
